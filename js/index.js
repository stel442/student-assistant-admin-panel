var working = false;
var loggedIn = false;
$('.login').on('submit', function(e) {
  e.preventDefault();
  if (working) return;
  working = true;
  var $this = $(this),
    $state = $this.find('button > .state');
  $this.addClass('loading');
  $state.html('Authenticating');
  //Email and password from login form
  var email = document.getElementById("email").value;
  var password =  document.getElementById("password").value;
firebase.auth().signInWithEmailAndPassword(email, password).then(function(result){
 //you can go back
 //location.href="home/index.html";
 //you cannot go back
 location.replace("home/index.html");
 $this.removeClass('loading');
 $state.html('Log in');
 working = false;
 loggedIn = true;


}).catch(function(error) {
// Handle Errors here.
var errorCode = error.code;
var errorMessage = error.message;
alert(errorMessage);
$this.removeClass('loading');
$state.html('Log in');
working = false;
console.log(errorMessage);

// ...
});

});

function logOut() {

  if (loggedIn){
  firebase.auth().signOut().then(function() {
    location.href="../index.html";
  // Sign-out successful.
}, function(error) {
  // An error happened.
  alert("couldnt log out");
});
}

}

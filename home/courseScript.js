var ref = firebase.database().ref().child("programs");
var selectedProg ="";
var selectedYear ="";
var selectedSem ="";
//var progSnapshot;

function showProg(){

var g = document.getElementById('programDivs');
for (var i = 0, len = g.children.length; i < len; i++)
{

    (function(index){
        g.children[i].onclick = function(){
              console.log(index)  ;
              var progDel =g.children[index].innerHTML;
             var progIndex = progDel.indexOf(">") + 1;
             var progIndexEnd = progDel.indexOf("/") -1 ;
           var progName = progDel.slice(progIndex,progIndexEnd);
           selectedProg = progName;
           document.getElementById("progName").innerHTML = progName;

              console.log(progName);
        }
    })(i);

}


}

function oneOne(){
  selectedYear="yearOne";

  selectedSem = "semOne";
  //this.addClass("col");
  //I know it a lame way of doing it :)
  document.getElementById("year1sem1").classList.add("activeSem");

  document.getElementById("year1sem2").classList.remove("activeSem");
  document.getElementById("year2sem1").classList.remove("activeSem");
  document.getElementById("year2sem2").classList.remove("activeSem");
  document.getElementById("year3sem1").classList.remove("activeSem");
  document.getElementById("year3sem2").classList.remove("activeSem");
  document.getElementById("year4sem1").classList.remove("activeSem");
  document.getElementById("year4sem2").classList.remove("activeSem");
}

function oneTwo(){
  selectedYear="yearOne";

  selectedSem = "semTwo";

  document.getElementById("year1sem2").classList.add("activeSem");

  document.getElementById("year1sem1").classList.remove("activeSem");
  document.getElementById("year2sem1").classList.remove("activeSem");
  document.getElementById("year2sem2").classList.remove("activeSem");
  document.getElementById("year3sem1").classList.remove("activeSem");
  document.getElementById("year3sem2").classList.remove("activeSem");
  document.getElementById("year4sem1").classList.remove("activeSem");
  document.getElementById("year4sem2").classList.remove("activeSem");
}

function twoOne(){
  selectedYear="yearTwo";

  selectedSem = "semOne";

  document.getElementById("year2sem1").classList.add("activeSem");

  document.getElementById("year1sem2").classList.remove("activeSem");
  document.getElementById("year1sem1").classList.remove("activeSem");
  document.getElementById("year2sem2").classList.remove("activeSem");
  document.getElementById("year3sem1").classList.remove("activeSem");
  document.getElementById("year3sem2").classList.remove("activeSem");
  document.getElementById("year4sem1").classList.remove("activeSem");
  document.getElementById("year4sem2").classList.remove("activeSem");
}

function twoTwo(){
  selectedYear="yearTwo";

  selectedSem = "semTwo";

  document.getElementById("year2sem2").classList.add("activeSem");

  document.getElementById("year1sem2").classList.remove("activeSem");
  document.getElementById("year2sem1").classList.remove("activeSem");
  document.getElementById("year1sem1").classList.remove("activeSem");
  document.getElementById("year3sem1").classList.remove("activeSem");
  document.getElementById("year3sem2").classList.remove("activeSem");
  document.getElementById("year4sem1").classList.remove("activeSem");
  document.getElementById("year4sem2").classList.remove("activeSem");
}

function threeOne(){
  selectedYear="yearThree";

  selectedSem = "semOne";

  document.getElementById("year3sem1").classList.add("activeSem");

  document.getElementById("year1sem2").classList.remove("activeSem");
  document.getElementById("year2sem1").classList.remove("activeSem");
  document.getElementById("year2sem2").classList.remove("activeSem");
  document.getElementById("year1sem1").classList.remove("activeSem");
  document.getElementById("year3sem2").classList.remove("activeSem");
  document.getElementById("year4sem1").classList.remove("activeSem");
  document.getElementById("year4sem2").classList.remove("activeSem");
}

function threeTwo(){
  selectedYear="yearThree";

  selectedSem = "semTwo";

  document.getElementById("year3sem2").classList.add("activeSem");

  document.getElementById("year1sem2").classList.remove("activeSem");
  document.getElementById("year2sem1").classList.remove("activeSem");
  document.getElementById("year2sem2").classList.remove("activeSem");
  document.getElementById("year3sem1").classList.remove("activeSem");
  document.getElementById("year1sem1").classList.remove("activeSem");
  document.getElementById("year4sem1").classList.remove("activeSem");
  document.getElementById("year4sem2").classList.remove("activeSem");
}

function fourOne(){
  selectedYear="yearFour";

  selectedSem = "semOne";

  document.getElementById("year4sem1").classList.add("activeSem");

  document.getElementById("year1sem2").classList.remove("activeSem");
  document.getElementById("year2sem1").classList.remove("activeSem");
  document.getElementById("year2sem2").classList.remove("activeSem");
  document.getElementById("year3sem1").classList.remove("activeSem");
  document.getElementById("year3sem2").classList.remove("activeSem");
  document.getElementById("year1sem1").classList.remove("activeSem");
  document.getElementById("year4sem2").classList.remove("activeSem");
}

function fourTwo(){
  selectedYear="yearFour";

  selectedSem = "semTwo";

  document.getElementById("year4sem2").classList.add("activeSem");

  document.getElementById("year1sem2").classList.remove("activeSem");
  document.getElementById("year2sem1").classList.remove("activeSem");
  document.getElementById("year2sem2").classList.remove("activeSem");
  document.getElementById("year3sem1").classList.remove("activeSem");
  document.getElementById("year3sem2").classList.remove("activeSem");
  document.getElementById("year4sem1").classList.remove("activeSem");
  document.getElementById("year1sem1").classList.remove("activeSem");
}




function soTired() {
  if(selectedProg!="" &&  selectedYear!="" && selectedSem !="")
  {
  var newProgram = firebase.database().ref().child("courses").child(selectedProg).child(selectedYear).child(selectedSem).push();
  newProgram.set({
    courseCode: document.getElementById("courseCode").value,
    courseName: document.getElementById("courseName").value,
    courseDifficulty: document.getElementById("courseDifficulty").value
  });
document.getElementById("courseCode").value ="";
document.getElementById("courseName").value="";
document.getElementById("courseDifficulty").value="";
document.getElementById("updateCourseForm").classList.add("hide");
document.getElementById("updateCourses").classList.remove("hide");
  console.log("hey");
  return false;
}

else {alert("Program , Year or Course not selected\nPlease select them");
        return false;
}

}

function toggleUpdateForm(){
  document.getElementById("updateCourseForm").classList.remove("hide");
  document.getElementById("updateCourses").classList.add("hide");
}

function cancelCourseUpdate(){

  document.getElementById("updateCourseForm").classList.add("hide");
  document.getElementById("updateCourses").classList.remove("hide");
  return false;

}

function displayCourses(){
  if(selectedProg!="" &&  selectedYear!="" && selectedSem !="")
  { //show table
    document.getElementById("table").classList.remove("hide");
    //show table captions
    document.getElementById("tableCaption").innerHTML = selectedProg + "  " + selectedYear + " " + selectedSem


    var container = document.getElementById("table");
    var elements = container.getElementsByClassName("tableData");
    while (elements[0]) {
    elements[0].parentNode.removeChild(elements[0]);
        }
    firebase.database().ref().child("courses").child(selectedProg).child(selectedYear).child(selectedSem).once("value", function(snapshot) {
      for(key in snapshot.val() ){
        var tr =  document.createElement('tr');
        tr.setAttribute('class', 'tableData');
        tr.innerHTML = "<td class='courseCode'>" + snapshot.val()[key].courseCode + "</td><td>" + snapshot.val()[key].courseName + "</td><td><button class='btn btn-success' onclick='outline()'>course outline</button></td><td><i class='fa fa-trash' onclick='deleteCourse()' style='cursor:pointer;'></i></td>" ;
        document.getElementById("table").appendChild(tr);



        //console.log(snapshot.val()[key].courseCode);
      //  document.getElementById("tableCaption").innerHTML = selectedProg + "  " + selectedYear + " " + selectedSem;
      }
      ;

  }, function (error) {
     console.log("Error: " + error.code);
  });
  }

  else {alert("Program , Year or Course not selected\nPlease select them");}

  }





  function deleteCourse(){
  var g = document.getElementById('table');
  //console.log(g.children.length);

  for (var i = 0, len = g.children.length; i < len; i++)
  {

      (function(index){
          g.children[i].onclick = function(){
                var coursetd =g.children[index].innerHTML;
                var courseIndex = coursetd.indexOf(">") + 1;
                var courseIndexEnd = coursetd.indexOf("/") -1 ;
                var courseCodeDel = coursetd.slice(courseIndex,courseIndexEnd);
                document.getElementById("table").removeChild(g.children[index]);
                console.log(courseCodeDel);
        firebase.database().ref().child("courses").child(selectedProg).child(selectedYear).child(selectedSem).orderByChild('courseCode').equalTo(courseCodeDel)
      .once('value').then(function(snapshot) {
          snapshot.forEach(function(childSnapshot) {
          //remove each child
          firebase.database().ref().child("courses").child(selectedProg).child(selectedYear).child(selectedSem).child(childSnapshot.key).remove();
        //  console.log();
          console.log("worked");


      });
  });

          }
      })(i);


  }

  }

//Weird functions
  function outline() {

    var g = document.getElementById('table');

    for (var i = 0, len = g.children.length; i < len; i++)
    {

        (function(index){
            g.children[i].onclick = function(){
                  var coursetd =g.children[index].innerHTML;

                  var courseCode = coursetd.slice(coursetd.indexOf(">") + 1,coursetd.indexOf("/") -1);
                  var courseName = coursetd.slice(coursetd.indexOf("/") +8,coursetd.indexOf("button") -10);
                  console.log(courseCode);
                  var progInfo = [selectedProg,selectedYear,selectedSem];
                  var courseInfo = [courseCode, courseName];
                  //console.log(courseInfo);
                  localStorage.setItem( 'progInfo', progInfo );
                  localStorage.setItem( 'courseInfo', courseInfo );
                  location.href="outline.html";

            }
        })(i);


    }



  }

  function outlineLoad(){
    var myProgInfo = localStorage['progInfo'].split(',');
    var myCourseInfo = localStorage['courseInfo'].split(',');
    var ref = firebase.database().ref().child("outlines").child(myProgInfo[0]).child(myProgInfo[1]).child(myProgInfo[2]).child(myCourseInfo[0]);
    //console.log(myCourseInfo);
    //localStorage.removeItem( 'progInfo' );
  //  localStorage.removeItem( 'courseInfo' ); // Clear the localStorage
  //reading outlines from database
document.getElementById('outlineHead').innerHTML=myProgInfo[0] + '  |  ' + myProgInfo[1] + '  |  '+ myProgInfo[2];
document.getElementById('outlineCourse').innerHTML=myCourseInfo[0] + '  ---  ' + myCourseInfo[1] ;


ref.once("value", function(snapshot) {
  for(key in snapshot.val() ){
    var li =  document.createElement('li');
  // div.setAttribute('class', 'programDivs');
    //div.innerHTML = "<div onclick='showProg()'>" +snapshot.val()[key].name + "</div>" ;
    li.innerHTML = snapshot.val()[key].topic + "<i class='fa fa-trash' onclick='delTopic()' style='cursor:pointer;'></i>";
    document.getElementById("outList").appendChild(li);
  }


}, function (error) {
 console.log("Error: " + error.code);
});


  }

  function toggleOutlineForm() {

    document.getElementById("outlineForm").classList.remove("hide");
    document.getElementById("outlineBut").classList.add("hide");

  }

  function cancelOutline() {

      document.getElementById("outlineForm").classList.add("hide");
      document.getElementById("outlineBut").classList.remove("hide");
      return false;
  }

  function updateOutline(){
    var myProgInfo = localStorage['progInfo'].split(',');
    var myCourseInfo = localStorage['courseInfo'].split(',');
    if(document.getElementById('topicName').value != ''){
      var newProgram = firebase.database().ref().child("outlines").child(myProgInfo[0]).child(myProgInfo[1]).child(myProgInfo[2]).child(myCourseInfo[0]).push();
      newProgram.set({
        topic: document.getElementById("topicName").value,
        timeStamp:firebase.database.ServerValue.TIMESTAMP
      });
      var li =  document.createElement('li');
      li.innerHTML = document.getElementById("topicName").value + "<i class='fa fa-trash' onclick='delTopic()' style='cursor:pointer;'></i>";
      document.getElementById("outList").appendChild(li);
      document.getElementById("topicName").value='';

    return false;
    }

else
{
    alert("Input can't be empty");
    return false;
  }

  }


  function delTopic(){
    var g = document.getElementById('outList');
    var myProgInfo = localStorage['progInfo'].split(',');
    var myCourseInfo = localStorage['courseInfo'].split(',');
    var ref = firebase.database().ref().child("outlines").child(myProgInfo[0]).child(myProgInfo[1]).child(myProgInfo[2]).child(myCourseInfo[0]);
    //console.log(g.children.length);

    for (var i = 0, len = g.children.length; i < len; i++)
    {

        (function(index){
            g.children[i].onclick = function(){
                  var topic =g.children[index].innerHTML;
                  //var courseIndex = coursetd.indexOf(">") + 1;
                //  var courseIndexEnd = coursetd.indexOf("/") -1 ;
                  var topDel = topic.slice(0,topic.indexOf("<"));
                  g.removeChild(g.children[index]);
                  console.log(topDel);

          ref.orderByChild('topic').equalTo(topDel)
        .once('value').then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
            //remove each child
            ref.child(childSnapshot.key).remove();
          //  console.log();
            console.log("worked");


        });
    });


            }
        })(i);


    }
  }

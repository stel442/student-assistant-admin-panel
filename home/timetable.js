//fetching data from database to populate program select
var ref = firebase.database().ref().child("programs");
function loadPrograms(){
  ref.once("value", function(snapshot) {
    for(key in snapshot.val() ){
      var option = document.createElement('option');
      option.innerHTML = snapshot.val()[key].name ;
      document.getElementById("progSel").appendChild(option);
    }

}, function (error) {
   console.log("Error: " + error.code);
});

}

//functionality of x button on modal
document.getElementsByClassName("close")[0].onclick = function() {
    document.getElementById('prog').style.display = "none";
}

document.getElementsByClassName("close")[1].onclick = function() {
    document.getElementById('ye').style.display = "none";
}

document.getElementsByClassName("close")[2].onclick = function() {
    document.getElementById('sem').style.display = "none";
}

document.getElementsByClassName("close")[3].onclick = function() {
    document.getElementById('add').style.display = "none";
}


//showing modals
function programModal(){
  document.getElementById('prog').style.display = "block";
}

function yearModal(){
  document.getElementById('ye').style.display = "block";
}

function semesterModal(){
  document.getElementById('sem').style.display = "block";
}

function addTimetable(){
  var checkProg=document.getElementById('progInner').innerHTML;
  var checkYear =   document.getElementById('yearInner').innerHTML ;
  var checkSem = document.getElementById('semInner').innerHTML;
  if(checkProg!='Choose Program' && checkYear!='Choose Year' && checkSem!='Choose Semester'){
  document.getElementById('add').style.display = "block";
}

else alert("Please select program, year, sem");
}

//functionality of check sign on modal
document.getElementsByClassName("done")[0].onclick = function() {
    document.getElementById('progInner').innerHTML = document.getElementById('progSel').value;
    document.getElementById('prog').style.display = "none";

}

document.getElementsByClassName("done")[1].onclick = function() {
    document.getElementById('yearInner').innerHTML = document.getElementById('yaerSel').value;
    document.getElementById('ye').style.display = "none";

}

document.getElementsByClassName("done")[2].onclick = function() {
    document.getElementById('semInner').innerHTML = document.getElementById('semSel').value;
    document.getElementById('sem').style.display = "none";

}

//for check button in add timetable form
document.getElementsByClassName("done")[3].onclick = function() {
//storing values in form
    var day_of_week = document.getElementById('day_of_week').value;
    var course_name = document.getElementById('course_name').value;
    var course_code = document.getElementById('course_code').value;
    var start_time = document.getElementById('start_time').value;
    var end_time = document.getElementById('end_time').value;
    var course_venue = document.getElementById('course_venue').value;

//checks if values are empty
    if ( day_of_week != '' && course_name != '' && course_code!='' && start_time != '' && end_time != '' && course_venue != '' ) {
      //if not empty pushes it and resets the form
      var timetable = firebase.database().ref().child("timetable");
      var checkProg=document.getElementById('progInner').innerHTML;
      var checkYear =   document.getElementById('yearInner').innerHTML ;
      var checkSem = document.getElementById('semInner').innerHTML;
      var classSchedule = timetable.child(checkProg).child(checkYear).child(checkSem).child(day_of_week).push();
      classSchedule.set({
        course_name: course_name,
        course_code: course_code,
        start_time: start_time,
        end_time:end_time,
        course_venue:course_venue,
        timestamp:firebase.database.ServerValue.TIMESTAMP

      });

      document.getElementById("timetable_form").reset();
      document.getElementById('add').style.display = "none";



    }
//else alerts that all fields be filled
    else alert('Please fill all fields !!');

}


//fetching timetable
function fetchTimetable(){
  var timetable = firebase.database().ref().child("timetable");
  var checkProg=document.getElementById('progInner').innerHTML;
  var checkYear =   document.getElementById('yearInner').innerHTML ;
  var checkSem = document.getElementById('semInner').innerHTML;

  //checking if selected
  if(checkProg!='Choose Program' && checkYear!='Choose Year' && checkSem!='Choose Semester'){
    $('.timeData').remove();

  timetable.child(checkProg).child(checkYear).child(checkSem).once("value", function(snapshot) {

if(snapshot.val() != null){

//Monday
for(key in snapshot.val().Monday ){
  var td = document.createElement('td');
  td.setAttribute('class', 'timeData');
  td.innerHTML = snapshot.val().Monday[key].start_time + '  -  '+ snapshot.val().Monday[key].end_time+ '<br>' + snapshot.val().Monday[key].course_name + '<br>' + snapshot.val().Monday[key].course_code + '<br>' + snapshot.val().Monday[key].course_venue;
  document.getElementById("Monday").appendChild(td);
}

//Tuesday
for(key in snapshot.val().Tuesday ){
  var td = document.createElement('td');
  td.setAttribute('class', 'timeData');
  td.innerHTML = snapshot.val().Tuesday[key].start_time + '  -  '+ snapshot.val().Tuesday[key].end_time+ '<br>' + snapshot.val().Tuesday[key].course_name + '<br>' + snapshot.val().Tuesday[key].course_code + '<br>' + snapshot.val().Tuesday[key].course_venue;
  document.getElementById("Tuesday").appendChild(td);
}

//Wednesday
for(key in snapshot.val().Wednesday ){
  var td = document.createElement('td');
  td.setAttribute('class', 'timeData');
  td.innerHTML = snapshot.val().Wednesday[key].start_time + '  -  '+ snapshot.val().Wednesday[key].end_time+ '<br>' + snapshot.val().Wednesday[key].course_name + '<br>' + snapshot.val().Wednesday[key].course_code + '<br>' + snapshot.val().Wednesday[key].course_venue;
  document.getElementById("Wednesday").appendChild(td);
}

//Thursday
for(key in snapshot.val().Thursday ){
  var td = document.createElement('td');
  td.setAttribute('class', 'timeData');
  td.innerHTML = snapshot.val().Thursday[key].start_time + '  -  '+ snapshot.val().Thursday[key].end_time+ '<br>' + snapshot.val().Thursday[key].course_name + '<br>' + snapshot.val().Thursday[key].course_code + '<br>' + snapshot.val().Thursday[key].course_venue;
  document.getElementById("Thursday").appendChild(td);
}

//Friday
    for(key in snapshot.val().Friday ){
      var td = document.createElement('td');
      td.setAttribute('class', 'timeData');
      td.innerHTML = snapshot.val().Friday[key].start_time + '  -  '+ snapshot.val().Friday[key].end_time+ '<br>' + snapshot.val().Friday[key].course_name + '<br>' + snapshot.val().Friday[key].course_code + '<br>' + snapshot.val().Friday[key].course_venue;
      document.getElementById("Friday").appendChild(td);
    }

    //console.log(snapshot.val().Monday);
  }

//if null is returned change title to 'no timetable for the selected semester'
  else {

      document.getElementById('timeHead').innerHTML = 'No timetable for the selected semester';
}
}, function (error) {
   console.log("Error: " + error.code);
});

}

//if nothing selected alert user
else{
  alert("Please select program, year, sem");
}
}

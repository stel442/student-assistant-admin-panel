window.onload = function(){
    firebase.database().ref().child("events").once("value", function(snapshot) {
      if( snapshot.val() === null) {
        document.getElementById('availEvents').innerHTML="There are no upcoming Events"
      }

      else{
      for(key in snapshot.val() ){
        var div =  document.createElement('div');
       div.setAttribute('class', 'eventDivs');
       div.innerHTML = '<h5>'+snapshot.val()[key].eventName+'</h5>' + '<p style="font-style:italic;">'+snapshot.val()[key].eventAbout +'</p>' +snapshot.val()[key].eventDate + '  |  ' + snapshot.val()[key].eventTime +'<br>' + snapshot.val()[key].eventVenue + '<br><i class="fa fa-trash" onclick="deleteEvent()"></i>' ;
        document.getElementById("eventDisplay").appendChild(div);
      }

      console.log("worked");
}
  }, function (error) {
     console.log("Error: " + error.code);
  });
}



function showEventsForm(){
  document.getElementById('newEventForm').classList.remove("hide");
  document.getElementById('showEventsForm').classList.add("hide");
}

function updateEvent(){
  var eventName = document.getElementById('eventName').value;
  var eventAbout = document.getElementById('eventAbout').value;
  var eventDate = document.getElementById('eventDate').value;
  var eventTime = document.getElementById('eventTime').value;
  var eventVenue = document.getElementById('eventVenue').value;

  if(eventName!="" && eventDate!="" && eventTime!="" && eventVenue!=""  && eventAbout!="")
  {
    var newProgram = firebase.database().ref().child("events").push();
    newProgram.set({
      eventName: eventName,
      eventAbout: eventAbout,
      eventDate:eventDate,
      eventTime:eventTime,
      eventVenue:eventVenue,
      updateTs:firebase.database.ServerValue.TIMESTAMP
    });
    console.log('worked');
    document.getElementById('eventName').value = "";
    document.getElementById('eventAbout').value = "";
    document.getElementById('eventDate').value = "";
    document.getElementById('eventTime').value="";
    document.getElementById('eventVenue').value="";

  }

  else alert("please fill all fields");
}

function cancelEvent() {
  document.getElementById('newEventForm').classList.add("hide");
  document.getElementById('showEventsForm').classList.remove("hide");

}

 function deleteEvent(){
   var g = document.getElementById('eventDisplay');

 for (var i = 0, len = g.children.length; i < len; i++){

   (function(index){
       g.children[i].onclick = function(){
             var eventClicked =g.children[index].innerHTML;
             var eventIndex = eventClicked.indexOf(">") + 1;
             var eventIndexEnd = eventClicked.indexOf("/") -1 ;
             var eventDel = eventClicked.slice(eventIndex,eventIndexEnd);
             document.getElementById("eventDisplay").removeChild(g.children[index]);
             console.log(eventDel);
     firebase.database().ref().child("events").orderByChild('eventName').equalTo(eventDel)
   .once('value').then(function(snapshot) {
       snapshot.forEach(function(childSnapshot) {
       //remove each child
       firebase.database().ref().child("events").child(childSnapshot.key).remove();
     //  console.log();
       console.log(childSnapshot.key);


   });
});

       }
   })(i);

 }
 }
